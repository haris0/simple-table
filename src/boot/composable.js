import { api } from './axios';

const generateId = (datas) => {
  for (let i = 0; i < datas.length; i += 1) {
    datas[i].id = (datas[i].age * 10) + i;
    if (datas[i].squard) {
      generateId(datas[i].squard);
    }
  }
  return datas;
};

export const fetchData = async () => {
  try {
    const res = await api.get('/list');
    let { data } = res.data;
    data = generateId(data);
    return data;
  } catch (error) {
    throw new Error(error);
  }
};
