/* eslint-disable no-console */
import { fetchData } from '../../boot/composable';

export const fetch = async ({ commit }) => {
  let data = [];
  try {
    data = await fetchData();
  } catch (error) {
    console.log(error);
  }
  commit('setData', data);
};
